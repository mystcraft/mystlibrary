# Myst Library

A shared library for use with Mystcraft add-ons and the Mystcraft API. CurseForge project page at https://www.curseforge.com/minecraft/mc-mods/mystlibrary.

To use the word creation tool, visit https://mystcraft.gitlab.io/mystlibrary/.

You can add this library to your dev environment by adding

```
maven {
    name = "curseforge"
    url = "https://minecraft.curseforge.com/api/maven/"
}
```
to the **repositories** section of your **build.gradle** file if it is not already present. Then refer to it in your **dependencies** section as
```
mystlibrary:mystlibrary:1.12.2:0.0.3.0
```
Look at this project's **build.gradle** file for an example where this was done with the Mystcraft API.