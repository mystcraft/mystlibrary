package mystlibrary.exception;

/**
 * Thrown when a mod is missing in some situation. It is probably best to use a
 * child class.
 */
public class ModMissingException extends Exception {

    public ModMissingException() {
        super();
    }

    /**
     * Thrown when a block symbol JSON is found but the parent mod of the block is
     * missing.
     */
    public static class BlockSymbol extends ModMissingException {

        public final String blockId;

        public BlockSymbol(String blockId) {
            super();
            this.blockId = blockId;
        }

    }

}
