package mystlibrary.exception;

import mystlibrary.symbol.SymbolMetadata;
import net.minecraft.util.ResourceLocation;

/**
 * Thrown when there is an issue parsing an IAgeSymbol from a JSON source.
 */
public class SymbolBuildException extends Exception {

    public final String symbolId;
    public final String message;

    public SymbolBuildException(SymbolMetadata metadata, String message) {
        super();
        this.symbolId = metadata == null ? "unknown:unknown" : metadata.name.toString();
        this.message = message == null ? "" : message;
    }

    public SymbolBuildException(ResourceLocation symbolId, String message) {
        super();
        this.symbolId = symbolId == null ? "unknown:unknown" : symbolId.toString();
        this.message = message == null ? "" : message;
    }

}
