package mystlibrary.event;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;

import mystlibrary.symbol.SymbolLoader;
import mystlibrary.symbol.behavior.BlockStateBehavior;
import mystlibrary.symbol.behavior.ColorBehavior;
import mystlibrary.symbol.behavior.FloatModifierBehavior;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Handle events for this mod in a central location. Handlers should be short
 * (1-5 lines) or simple (the same call repeated with different arguments)
 */
@EventBusSubscriber
public final class EventHandler {

    @SubscribeEvent
    public static void registerBehaviors(BehaviorRegistrationEvent event) {
        // The Method Reference operator `::` allows us to pass these factory functions
        // into the registry
        event.registerBehavior(ColorBehavior.NAME, ColorBehavior::fromJson);
        event.registerBehavior(FloatModifierBehavior.NAME, FloatModifierBehavior::fromJson);
        event.registerBehavior(BlockStateBehavior.NAME, BlockStateBehavior::fromJson);
    }

    @SubscribeEvent
    public static void registerSymbols(RegistryEvent.Register<IAgeSymbol> event) {
        SymbolLoader loader = new SymbolLoader();
        MinecraftForge.EVENT_BUS.post(new BehaviorRegistrationEvent(loader));
        loader.registerSymbols(event.getRegistry());
    }

}
