package mystlibrary.event;

import mystlibrary.symbol.SymbolLoader;
import mystlibrary.symbol.behavior.IAgeSymbolBehavior.BehaviorFactory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.eventhandler.Event;

/**
 * Fires right before IAgeSymbol registration. Subscribe to this event to
 * register your own Behaviors. See
 * {@code mystlibrary.event.EventHandler::registerBehaviors} for how this occurs
 * with default behaviors.
 *
 */
public final class BehaviorRegistrationEvent extends Event {

	private final SymbolLoader loader;

	public BehaviorRegistrationEvent(SymbolLoader loader) {
		this.loader = loader;
	}

	/**
	 * Call this function during the event to register a BehaviorFactory to a
	 * ResourceLocation
	 * 
	 * @param behaviorName - A ResourceLocation that refers to this behavior.
	 * @param factory      - A function that takes a JSON object and SymbolMetadata
	 *                     and returns a new instance of this behavior. Any changes
	 *                     to the metadata will be reflected in the registered
	 *                     symbol. To pass in a reference to an existing function,
	 *                     use the `::` operator. For example
	 *                     `ColorBehavior::fromJson` refers to a static function
	 *                     called `fromJson` on a class named `ColorBehavior`.
	 */
	public void registerBehavior(ResourceLocation behaviorName, BehaviorFactory factory) {
		this.loader.registerBehavior(behaviorName, factory);
	}

}
