package mystlibrary;

import org.apache.logging.log4j.Logger;

import com.xcompwiz.mystcraft.api.MystObjects;

import mystlibrary.mystcraftapi.MystcraftApiWrapper;
import mystlibrary.proxy.CommonProxy;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * A mod to help add support for Mystcraft to other mods. Provides simple
 * opinionated defaults, commonly reused logic, and automatic data-driven
 * content loading.
 */
@Mod(modid = MystLibrary.MOD_ID, name = MystLibrary.NAME, version = MystLibrary.VERSION, dependencies = "after:mystcraft")
public final class MystLibrary {
    public static final String MOD_ID = "mystlibrary";
    public static final String NAME = "Myst Library";
    public static final String VERSION = "1.12.2-0.0.3.2";
    public static final String CLIENT_PROXY_CLASS = "mystlibrary.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "mystlibrary.proxy.ServerProxy";

    /**
     * Myst Library's dedicated Logger. Do not use before preInit, as it will be
     * {@code null}.
     */
    public static Logger logger;

    /**
     * Provides access to latest Mystcraft API instances. Each instance may be null,
     * so be sure to check.
     */
    public static MystcraftApiWrapper mystcraft;

    @SidedProxy(clientSide = CLIENT_PROXY_CLASS, serverSide = SERVER_PROXY_CLASS)
    public static CommonProxy proxy;

    private static boolean isMystcraftLoaded;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        isMystcraftLoaded = Loader.isModLoaded(MystObjects.MystcraftModId);
        logger = event.getModLog();
        proxy.preInit(event);
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

    /**
     * A convenience method to determine if Mystcraft is a currently loaded mod.
     * 
     * @return Whether or not Mystcraft is a currently loaded mod.
     */
    public static boolean isMystcraftLoaded() {
        return isMystcraftLoaded;
    }

    /**
     * @return A new ResourceLocation from a given name, with MystLibrary's modid
     */
    public static ResourceLocation resource(String resourceName) {
        return new ResourceLocation(MOD_ID, resourceName);
    }

}
