package mystlibrary.symbol.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.util.Color;

import mystlibrary.MystLibrary;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;

/**
 * Contains maps that connect symbol ResourceLocations to the objects they
 * represent. Use the Symbol API at {@link MystLibrary.mystcraft.symbol} to
 * convert a ResourceLocation into an IAgeSymbol.
 */
public final class SymbolCache {
	private SymbolCache() {
	}

	private static Map<ResourceLocation, Object> mapAllSymbols = new HashMap<ResourceLocation, Object>();
	private static Map<ResourceLocation, Biome> mapBiomes = new HashMap<ResourceLocation, Biome>();
	private static Map<ResourceLocation, IBlockState> mapBlocks = new HashMap<ResourceLocation, IBlockState>();
	private static Map<ResourceLocation, Color> mapColors = new HashMap<ResourceLocation, Color>();
	private static Map<ResourceLocation, Number> mapDirections = new HashMap<ResourceLocation, Number>();
	private static Map<ResourceLocation, Number> mapLengths = new HashMap<ResourceLocation, Number>();
	private static Map<ResourceLocation, Number> mapPhases = new HashMap<ResourceLocation, Number>();

	/**
	 * Adds all registered symbols to the symbol caches located here.
	 */
	public static void cacheAllSymbols() {
		List<IAgeSymbol> symbols = MystLibrary.mystcraft.symbol.getAllRegisteredSymbols();
		for (IAgeSymbol symbol : symbols) {
			if (SymbolConverter.toBiome(symbol, false) != null) {
				addToBiomeSymbols(symbol, SymbolConverter.toBiome(symbol, true));
			} else if (SymbolConverter.toBlockState(symbol, false) != null) {
				addToBlockSymbols(symbol, SymbolConverter.toBlockState(symbol, true));
			} else if (SymbolConverter.toColor(symbol, false) != null) {
				addToColorSymbols(symbol, SymbolConverter.toColor(symbol, true));
			} else if (SymbolConverter.toDirection(symbol, false) != null) {
				addToDirectionSymbols(symbol, SymbolConverter.toDirection(symbol, true));
			} else if (SymbolConverter.toLength(symbol, false) != null) {
				addToLengthSymbols(symbol, SymbolConverter.toLength(symbol, true));
			} else if (SymbolConverter.toPhase(symbol, false) != null) {
				addToPhaseSymbols(symbol, SymbolConverter.toPhase(symbol, true));
			}
		}
	}

	/**
	 * Adds symbol to cache of all symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToAllSymbols(ResourceLocation loc, Object obj) {
		mapAllSymbols.put(loc, obj);
	}

	/**
	 * Adds symbol to cache of all symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToAllSymbols(IAgeSymbol sym, Object obj) {
		addToAllSymbols(sym.getRegistryName(), obj);
	}

	/**
	 * Adds symbol to cache of biome symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToBiomeSymbols(ResourceLocation loc, Biome obj) {
		mapBiomes.put(loc, obj);
		addToAllSymbols(loc, obj);
	}

	/**
	 * Adds symbol to cache of biome symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToBiomeSymbols(IAgeSymbol sym, Biome obj) {
		addToBiomeSymbols(sym.getRegistryName(), obj);
	}

	/**
	 * Adds symbol to cache of block symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToBlockSymbols(ResourceLocation loc, IBlockState obj) {
		mapBlocks.put(loc, obj);
		addToAllSymbols(loc, obj);
	}

	/**
	 * Adds symbol to cache of block symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToBlockSymbols(IAgeSymbol sym, IBlockState obj) {
		addToBlockSymbols(sym.getRegistryName(), obj);
	}

	/**
	 * Adds symbol to cache of color symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToColorSymbols(ResourceLocation loc, Color obj) {
		mapColors.put(loc, obj);
		addToAllSymbols(loc, obj);
	}

	/**
	 * Adds symbol to cache of color symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToColorSymbols(IAgeSymbol sym, Color obj) {
		addToColorSymbols(sym.getRegistryName(), obj);
	}

	/**
	 * Adds symbol to cache of direction symbols located here. If it is already
	 * cached, nothing happens.
	 */
	public static void addToDirectionSymbols(ResourceLocation loc, Number obj) {
		mapDirections.put(loc, obj);
		addToAllSymbols(loc, obj);
	}

	/**
	 * Adds symbol to cache of direction symbols located here. If it is already
	 * cached, nothing happens.
	 */
	public static void addToDirectionSymbols(IAgeSymbol sym, Number obj) {
		addToDirectionSymbols(sym.getRegistryName(), obj);
	}

	/**
	 * Adds symbol to cache of length symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToLengthSymbols(ResourceLocation loc, Number obj) {
		mapLengths.put(loc, obj);
		addToAllSymbols(loc, obj);
	}

	/**
	 * Adds symbol to cache of length symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToLengthSymbols(IAgeSymbol sym, Number obj) {
		addToLengthSymbols(sym.getRegistryName(), obj);
	}

	/**
	 * Adds symbol to cache of phase symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToPhaseSymbols(ResourceLocation loc, Number obj) {
		mapPhases.put(loc, obj);
		addToAllSymbols(loc, obj);
	}

	/**
	 * Adds symbol to cache of phase symbols located here. If it is already cached,
	 * nothing happens.
	 */
	public static void addToPhaseSymbols(IAgeSymbol sym, Number obj) {
		addToPhaseSymbols(sym.getRegistryName(), obj);
	}

	/**
	 * Use to access all cached symbols.
	 * 
	 * @return A HashMap connecting symbol ResourceLocations to the Objects they
	 *         correspond to.
	 */
	public static Map<ResourceLocation, Object> getAllCachedSymbols() {
		return new HashMap<>(mapAllSymbols);
	}

	/**
	 * Use to access all cached biome symbols.
	 * 
	 * @return A HashMap connecting symbol ResourceLocations to the Biomes they
	 *         correspond to.
	 */
	public static Map<ResourceLocation, Biome> getCachedBiomeSymbols() {
		return new HashMap<>(mapBiomes);
	}

	/**
	 * Use to access all cached block symbols.
	 * 
	 * @return A HashMap connecting symbol ResourceLocations to the IBlockStates
	 *         they correspond to.
	 */
	public static Map<ResourceLocation, IBlockState> getCachedBlockSymbols() {
		return new HashMap<>(mapBlocks);
	}

	/**
	 * Use to access all cached color symbols.
	 * 
	 * @return A HashMap connecting symbol ResourceLocations to the Colors they
	 *         correspond to.
	 */
	public static Map<ResourceLocation, Color> getCachedColorSymbols() {
		return new HashMap<>(mapColors);
	}

	/**
	 * Use to access all cached direction symbols.
	 * 
	 * @return A HashMap connecting symbol ResourceLocations to the Directions they
	 *         correspond to.
	 */
	public static Map<ResourceLocation, Number> getCachedDirectionSymbols() {
		return new HashMap<>(mapDirections);
	}

	/**
	 * Use to access all cached length symbols.
	 * 
	 * @return A HashMap connecting symbol ResourceLocations to the Lengths they
	 *         correspond to.
	 */
	public static Map<ResourceLocation, Number> getCachedLengthSymbols() {
		return new HashMap<>(mapLengths);
	}

	/**
	 * Use to access all cached phase symbols.
	 * 
	 * @return A HashMap connecting symbol ResourceLocations to the Phases they
	 *         correspond to.
	 */
	public static Map<ResourceLocation, Number> getCachedPhaseSymbols() {
		return new HashMap<>(mapPhases);
	}
}
