package mystlibrary.symbol.util;

import java.util.List;

import com.xcompwiz.mystcraft.api.symbol.BlockCategory;
import com.xcompwiz.mystcraft.api.symbol.BlockDescriptor;
import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.symbol.ModifierUtils;
import com.xcompwiz.mystcraft.api.util.Color;
import com.xcompwiz.mystcraft.api.util.ColorGradient;
import com.xcompwiz.mystcraft.api.world.logic.IBiomeController;
import com.xcompwiz.mystcraft.api.world.logic.ICelestial;
import com.xcompwiz.mystcraft.api.world.logic.IEnvironmentalEffect;
import com.xcompwiz.mystcraft.api.world.logic.ILightingController;
import com.xcompwiz.mystcraft.api.world.logic.IPopulate;
import com.xcompwiz.mystcraft.api.world.logic.Modifier;

import mystlibrary.MystLibrary;
import mystlibrary.dummy.AgeDirectorDummy;
import net.minecraft.block.state.IBlockState;
import net.minecraft.world.biome.Biome;

/**
 * Convert a symbol to various other types
 */
public final class SymbolConverter {
	private SymbolConverter() {
	}

	public static Color toColor(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		Color color = null;

		try {
			ageDirector.register(symbol);
			Modifier colorModifier = ageDirector.popModifier(ModifierUtils.COLOR);
			if (colorModifier != null) {
				color = colorModifier.asColor();
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return color;
	}

	public static Number toLength(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		Number length = null;

		try {
			ageDirector.register(symbol);
			Modifier lengthModifier = ageDirector.popModifier(ModifierUtils.FACTOR);
			if (lengthModifier != null) {
				length = lengthModifier.asNumber();
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return length;
	}

	public static Number toDirection(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		Number direction = null;

		try {
			ageDirector.register(symbol);
			Modifier directionModifier = ageDirector.popModifier(ModifierUtils.ANGLE);
			if (directionModifier != null) {
				direction = directionModifier.asNumber();
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return direction;
	}

	public static Number toPhase(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		Number phase = null;

		try {
			ageDirector.register(symbol);
			Modifier phaseModifier = ageDirector.popModifier(ModifierUtils.PHASE);
			if (phaseModifier != null) {
				phase = phaseModifier.asNumber();
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return phase;
	}

	public static IBlockState toBlockState(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		IBlockState state = null;

		try {
			ageDirector.register(symbol);
			BlockDescriptor descriptor = ModifierUtils.popBlockMatching(ageDirector, BlockCategory.ANY);
			if (descriptor != null) {
				state = descriptor.blockstate;
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return state;
	}

	public static Biome toBiome(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		Biome biome = null;

		try {
			ageDirector.register(symbol);
			biome = ModifierUtils.popBiome(ageDirector);
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return biome;
	}

	public static ColorGradient toGradient(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		ColorGradient gradient = null;

		try {
			ageDirector.register(symbol);
			gradient = ModifierUtils.popGradient(ageDirector);
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		if (gradient.getLength() <= 0) {
			return null;
		}
		return gradient;
	}

	public static ICelestial toCelestial(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		ICelestial celestial = null;

		try {
			ageDirector.register(symbol);
			List<ICelestial> celestials = ageDirector.celestials;
			if (celestials.size() > 0) {
				celestial = celestials.get(0);
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return celestial;
	}

	public static IBiomeController toBiomeController(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		IBiomeController biomeController = null;

		try {
			ageDirector.register(symbol);
			List<IBiomeController> biomeControllers = ageDirector.biomeControllers;
			if (biomeControllers.size() > 0) {
				biomeController = biomeControllers.get(0);
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return biomeController;
	}

	public static IEnvironmentalEffect toEnvironmentalEffect(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		IEnvironmentalEffect environmentalEffect = null;

		try {
			ageDirector.register(symbol);
			List<IEnvironmentalEffect> environmentalEffects = ageDirector.environmentalEffects;
			if (environmentalEffects.size() > 0) {
				environmentalEffect = environmentalEffects.get(0);
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return environmentalEffect;
	}

	public static ILightingController toLightingController(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		ILightingController lightingController = null;

		try {
			ageDirector.register(symbol);
			List<ILightingController> lightingControllers = ageDirector.lightingControllers;
			if (lightingControllers.size() > 0) {
				lightingController = lightingControllers.get(0);
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return lightingController;
	}

	public static IPopulate toPopulator(IAgeSymbol symbol, boolean logFailure) {
		if (symbol == null)
			return null;

		AgeDirectorDummy ageDirector = new AgeDirectorDummy();
		IPopulate populator = null;

		try {
			ageDirector.register(symbol);
			List<IPopulate> populators = ageDirector.populators;
			if (populators.size() > 0) {
				populator = populators.get(0);
			}
		} catch (Exception e) {
			if (logFailure) {
				MystLibrary.logger.warn(e);
			}
		}

		return populator;
	}
}
