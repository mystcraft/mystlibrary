package mystlibrary.symbol.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mystlibrary.exception.SymbolBuildException;
import mystlibrary.symbol.SymbolMetadata;

public class SymbolJsonParser {

    public static int parseInt(JsonObject jsonObject, String fieldName, SymbolMetadata metadata)
            throws SymbolBuildException {

        int value = 0;
        JsonElement element = jsonObject.get(fieldName);

        if (element == null) {
            throw new SymbolBuildException(metadata,
                    "Could not locate expected field \"" + fieldName + "\": " + jsonObject.toString());
        }

        if (element.isJsonArray()) {
            throw new SymbolBuildException(metadata, "Field \"" + fieldName
                    + "\" should be a single int value, but it is an array: " + element.toString());
        }

        try {
            value = element.getAsInt();
        } catch (ClassCastException ex) {
            throw new SymbolBuildException(metadata,
                    "Cannot cast field \"" + fieldName + "\" to int: " + element.toString());
        } catch (IllegalStateException ex) {
            // How did you even get here?
            throw new SymbolBuildException(metadata, "Field \"" + fieldName
                    + "\" should be a single int value, but it is an array: " + element.toString());

        }

        return value;

    }

    public static float parseFloat(JsonObject jsonObject, String fieldName, SymbolMetadata metadata)
            throws SymbolBuildException {

        float value = 0.0F;
        JsonElement element = jsonObject.get(fieldName);

        if (element == null) {
            throw new SymbolBuildException(metadata,
                    "Could not locate expected field \"" + fieldName + "\": " + jsonObject.toString());
        }

        if (element.isJsonArray()) {
            throw new SymbolBuildException(metadata, "Field \"" + fieldName
                    + "\" should be a single float value, but it is an array: " + element.toString());
        }

        try {
            value = element.getAsFloat();
        } catch (ClassCastException ex) {
            throw new SymbolBuildException(metadata,
                    "Cannot cast field \"" + fieldName + "\" to float: " + element.toString());
        } catch (IllegalStateException ex) {
            // How did you even get here?
            throw new SymbolBuildException(metadata, "Field \"" + fieldName
                    + "\" should be a single float value, but it is an array: " + element.toString());

        }

        return value;

    }

    public static String parseString(JsonObject jsonObject, String fieldName, SymbolMetadata metadata)
            throws SymbolBuildException {

        String value = "";
        JsonElement element = jsonObject.get(fieldName);

        if (element == null) {
            throw new SymbolBuildException(metadata,
                    "Could not locate expected field \"" + fieldName + "\": " + jsonObject.toString());
        }

        if (element.isJsonArray()) {
            throw new SymbolBuildException(metadata, "Field \"" + fieldName
                    + "\" should be a single string value, but it is an array: " + element.toString());
        }

        try {
            value = element.getAsString();
        } catch (ClassCastException ex) {
            throw new SymbolBuildException(metadata,
                    "Cannot cast field \"" + fieldName + "\" to string: " + element.toString());
        } catch (IllegalStateException ex) {
            // How did you even get here?
            throw new SymbolBuildException(metadata, "Field \"" + fieldName
                    + "\" should be a single string value, but it is an array: " + element.toString());

        }

        return value;

    }

    public static JsonArray parseJsonArray(JsonObject jsonObject, String fieldName, SymbolMetadata metadata)
            throws SymbolBuildException {

        JsonArray jsonArray = new JsonArray();
        JsonElement element = jsonObject.get(fieldName);

        if (element == null) {
            throw new SymbolBuildException(metadata,
                    "Could not locate expected field \"" + fieldName + "\": " + jsonObject.toString());
        }

        try {
            jsonArray = element.getAsJsonArray();
        } catch (IllegalStateException ex) {
            throw new SymbolBuildException(metadata,
                    "Field \"" + fieldName + "\" should be a JsonArray, but it is not: " + element.toString());

        }

        return jsonArray;

    }

    public static JsonObject parseJsonObject(JsonObject mainObject, String fieldName, SymbolMetadata metadata)
            throws SymbolBuildException {

        JsonObject childObject = new JsonObject();
        JsonElement element = mainObject.get(fieldName);

        if (element == null) {
            throw new SymbolBuildException(metadata,
                    "Could not locate expected field \"" + fieldName + "\": " + mainObject.toString());
        }

        try {
            childObject = element.getAsJsonObject();
        } catch (IllegalStateException ex) {
            throw new SymbolBuildException(metadata,
                    "Field \"" + fieldName + "\" should be a JsonObject, but it is not: " + element.toString());

        }

        return childObject;

    }

}
