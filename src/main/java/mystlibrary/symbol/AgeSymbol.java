package mystlibrary.symbol;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;
import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.world.AgeDirector;

import mystlibrary.MystLibrary;
import mystlibrary.exception.ModMissingException;
import mystlibrary.exception.SymbolBuildException;
import mystlibrary.grammar.GrammarRule;
import mystlibrary.proxy.CommonProxy;
import mystlibrary.symbol.behavior.IAgeSymbolBehavior;
import mystlibrary.symbol.behavior.IAgeSymbolBehavior.BehaviorFactory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Provides an opinionated implementation of IAgeSymbol that handles grammar and
 * rarity registration, localization, and more.
 *
 * While you can create your own instances of this class, it is recommended that
 * you include new symbols as JSON assets rather than code. This mod will
 * automatically discover and register new symbol JSON files from
 * `assets/[modid]/agesymbols/[name].json`. It will also discover symbols in
 * `config/mystcraft/agesymbols/[name].json` to allow server customization.
 *
 * To add a new symbol behavior, extend `IAgeSymbolBehavior` and register that
 * behavior via `AgeSymbol.registerBehavior`.
 *
 * @see mystlibrary.symbol.behavior.IAgeSymbolBehavior
 * @see com.xcompwiz.mystcraft.api.symbol.IAgeSymbol
 */
public final class AgeSymbol implements IAgeSymbol {

    private final SymbolMetadata metadata;
    private final IAgeSymbolBehavior behavior;

    /**
     * Called during symbol registration to build new AgeSymbol instances from JSON.
     */
    public static AgeSymbol fromJson(JsonObject json) throws SymbolBuildException, ModMissingException.BlockSymbol {
        AgeSymbol symbol = null;

        // Parse symbol metadata
        SymbolMetadata metadata = SymbolMetadata.fromJson(json);

        // Parse symbol behavior sub-object
        IAgeSymbolBehavior behavior = null;
        JsonObject behaviorJson = json.get("behavior").getAsJsonObject();

        // Parse symbol behavior name
        String behaviorId = behaviorJson.get(IAgeSymbolBehavior.BEHAVIOR_ID).getAsString();
        ResourceLocation behaviorName = new ResourceLocation(behaviorId);

        // Parse symbol behavior via factory
        BehaviorFactory factory = SymbolLoader.BEHAVIORS.get(behaviorName);
        if (factory == null) {
            factory = (j, m) -> new NoBehavior();
            MystLibrary.logger.error(
                    "Could not parse behavior \"" + behaviorName.toString() + "\". Replacing with dummy behavior.");
        }
        behavior = factory.fromJson(behaviorJson, metadata);
        symbol = new AgeSymbol(metadata, behavior);

        return symbol;
    }

    /**
     * @return A JsonObject representing this symbol
     */
    public JsonObject toJson() {
        JsonObject json = metadata.toJson();
        json.add("behavior", behavior.toJson());

        return json;
    }

    /**
     * @return A JsonObject representing the symbol, null if one cannot be created
     */
    public static JsonObject toJson(IAgeSymbol symbol) {
        if (symbol instanceof AgeSymbol) {
            return ((AgeSymbol) symbol).toJson();
        } else {
            return null;
        }
    }

    public AgeSymbol(SymbolMetadata metadata, IAgeSymbolBehavior behavior) {
        this.metadata = new SymbolMetadata(metadata);
        this.behavior = behavior;
    }

    /**
     * Called during symbol registration to add Grammar Rules for this symbol.
     */
    public void registerGrammarRules() {
        for (GrammarRule rule : metadata.grammarRules) {
            MystLibrary.mystcraft.grammar.registerGrammarRule(rule.parent, rule.rank, rule.getChildren());
        }
    }

    /**
     * Called during symbol registration to set symbol rarity.
     */
    public void registerSymbolRank() {
        if (metadata.cardRank != null) {
            MystLibrary.mystcraft.symbolValues.setSymbolCardRank(this, metadata.cardRank);
        }
    }

    @Override
    public ResourceLocation getRegistryName() {
        return metadata.name;
    }

    @Override
    public IAgeSymbol setRegistryName(ResourceLocation name) {
        metadata.name = name;

        return this;
    }

    @Override
    public void registerLogic(AgeDirector controller, long seed) {
        behavior.registerLogic(controller, seed);
    }

    @Override
    public int instabilityModifier(int count) {
        int instability = 0;

        if (metadata.maxCount != null && metadata.instabilityPerInstance != null) {
            if (count > metadata.maxCount.intValue()) {
                instability = metadata.instabilityPerInstance.intValue();
            }
        }

        return instability;
    }

    @Override
    public boolean generatesConfigOption() {
        return true;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public String getLocalizedName() {
        CommonProxy proxy = MystLibrary.proxy;

        List<String> localizedParameters = new ArrayList<>();
        for (String param : metadata.localizationParameters) {
            localizedParameters.add(proxy.localize(param));
        }

        return proxy.localize(metadata.unlocalName, localizedParameters.toArray(new Object[0]));
    }

    @Override
    public String[] getPoem() {
        return metadata.poem.toArray(new String[0]);
    }

    /**
     * A placeholder behavior that does nothing. An IAgeSymbol will be created, but
     * it will cause no effects.
     */
    private static final class NoBehavior implements IAgeSymbolBehavior {

        @Override
        public void registerLogic(AgeDirector controller, long seed) {
        }

        @Override
        public JsonObject toJson() {
            JsonObject json = new JsonObject();

            json.addProperty("behavior_id", "unknown:unknown");

            return json;
        }
    }

}
