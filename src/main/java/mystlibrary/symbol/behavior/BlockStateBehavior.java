package mystlibrary.symbol.behavior;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.xcompwiz.mystcraft.api.symbol.BlockCategory;
import com.xcompwiz.mystcraft.api.symbol.BlockDescriptor;
import com.xcompwiz.mystcraft.api.symbol.ModifierUtils;
import com.xcompwiz.mystcraft.api.world.AgeDirector;
import com.xcompwiz.mystcraft.instability.InstabilityBlockManager;

import mystlibrary.MystLibrary;
import mystlibrary.exception.ModMissingException;
import mystlibrary.exception.SymbolBuildException;
import mystlibrary.grammar.GrammarRule;
import mystlibrary.localization.Localization;
import mystlibrary.symbol.SymbolMetadata;
import mystlibrary.symbol.util.SymbolJsonParser;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;

/**
 * Pushes a block descriptor to the modifier stack when used.
 */
public class BlockStateBehavior implements IAgeSymbolBehavior {

    public final BlockDescriptor blockDescriptor;
    private static final String BLOCK_ID = "block_id";
    private static final String PROPERTIES = "properties";
    private static final String INSTABILITY_BASE = "instability_base";
    private static final String INSTABILITY_EXPOSED = "instablity_exposed";
    private static final String BLOCK_PLACEHOLDER = "BLOCK_NAME";
    public static final ResourceLocation NAME = MystLibrary.resource("block");

    public static IAgeSymbolBehavior fromJson(JsonObject json, SymbolMetadata metadata)
            throws SymbolBuildException, ModMissingException.BlockSymbol {
        BlockStateBehavior behavior = null;

        String blockId = json.get(BLOCK_ID).getAsString();
        Block block = Block.getBlockFromName(blockId);
        if (!Loader.isModLoaded(new ResourceLocation(blockId).getResourceDomain())) {
            throw new ModMissingException.BlockSymbol(blockId);
        } else if (block == null) {
            throw new SymbolBuildException(metadata, "Block \"" + blockId + "\" not found.");
        }
        IBlockState state = block.getDefaultState();

        // Get specific IBlockState from properties.
        if (json.get(PROPERTIES) != null) {
            JsonObject blockProperties = json.get(PROPERTIES).getAsJsonObject();
            for (Entry<String, JsonElement> entry : blockProperties.entrySet()) {
                String propertyId = entry.getKey();
                String propertyValue = entry.getValue().getAsString();
                IProperty property = parseProperty(state, propertyId);
                if (property == null) {
                    MystLibrary.logger.error("Cannot find property \"" + propertyId + "\" for block \"" + blockId
                            + "\". Leaving in default state.");
                    continue;
                }
                Comparable value = null;
                Collection<?> possibleValues = property.getAllowedValues();
                List<String> acceptableValues = new ArrayList<String>();
                for (Object singleValue : possibleValues) {
                    if (singleValue instanceof IStringSerializable) {
                        String valueName = ((IStringSerializable) singleValue).getName().trim().toLowerCase();
                        if (valueName.equals(propertyValue)) {
                            value = (Comparable) property.parseValue(valueName).get();
                            break;
                        }
                        acceptableValues.add(valueName);
                    } else {
                        acceptableValues.add(singleValue.toString().trim().toLowerCase());
                    }
                    if (singleValue.toString().equals(propertyValue)) {
                        value = (Comparable) singleValue;
                        break;
                    }
                }
                if (value == null) {
                    MystLibrary.logger.error("Cannot apply value \"" + propertyValue + "\" to property \"" + propertyId
                            + "\". Leaving in default state.");
                    MystLibrary.logger.error("Acceptable values are: " + String.join(", ", acceptableValues) + ".");
                    continue;
                }
                state = state.withProperty(property, value);
            }
        }

        // Parse instability values.
        float instabilityFactorBase = json.get(INSTABILITY_BASE) != null
                ? SymbolJsonParser.parseFloat(json, INSTABILITY_BASE, metadata)
                : 0.0F;
        float instabilityFactorExposed = json.get(INSTABILITY_EXPOSED) != null
                ? SymbolJsonParser.parseFloat(json, INSTABILITY_EXPOSED, metadata)
                : 0.0F;

        // Get block categories.
        List<BlockCategory> categories = new ArrayList<BlockCategory>();
        for (GrammarRule rule : metadata.grammarRules) {
            BlockCategory category = BlockCategory.getBlockCategory(rule.parent);
            categories.add(category);
        }

        // Generate unlocalized name from blockstate if one was not specified.
        if (metadata.unlocalName.isEmpty()) {
            metadata.unlocalName = Localization.getUnlocalizedBlockStateName(state).endsWith(".name")
                    ? Localization.getUnlocalizedBlockStateName(state)
                    : Localization.getUnlocalizedBlockStateName(state) + ".name";
        }

        // Generate the last poem word if the placeholder was used.
        for (int i = 0; i < metadata.poem.size(); i++) {
            if (metadata.poem.get(i).equals(BLOCK_PLACEHOLDER)) {
                metadata.poem.set(i, Localization.getLocalizedBlockStateName(state));
            }
        }

        behavior = new BlockStateBehavior(state, categories, instabilityFactorBase, instabilityFactorExposed);

        return behavior;
    }

    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        IBlockState state = blockDescriptor.blockstate;

        json.addProperty(IAgeSymbolBehavior.BEHAVIOR_ID, NAME.toString());

        json.addProperty(BLOCK_ID, state.getBlock().getRegistryName().toString());

        JsonObject properties = new JsonObject();
        json.add(PROPERTIES, properties);

        Collection<IProperty<?>> propertiesCollection = state.getPropertyKeys();
        for (IProperty<?> property : propertiesCollection) {
            Comparable<?> value = state.getValue(property);

            if (value instanceof Number) {
                properties.addProperty(property.getName(), (Number) value);
            } else if (value instanceof Boolean) {
                properties.addProperty(property.getName(), (Boolean) value);
            } else if (value instanceof String) {
                properties.addProperty(property.getName(), (String) value);
            } else if (value instanceof IStringSerializable) {
                properties.addProperty(property.getName(), ((IStringSerializable) value).getName());
            }
        }

        return json;
    }

    /**
     * Convenience constructor that lets you use an IBlockState instead of a
     * BlockDescripor as an argument.
     * 
     * @param state The IBlockState that will be used to create a BlockDescriptor.
     * @param categories The block categories this IBlockState will be valid for.
     */
    public BlockStateBehavior(final IBlockState state, List<BlockCategory> categories, float instabilityBase,
            float instabilityExposed) {
        this(new BlockDescriptor(state), categories, instabilityBase, instabilityExposed);
    }

    public BlockStateBehavior(final BlockDescriptor block, List<BlockCategory> categories, float instabilityBase,
            float instabilityExposed) {
        this.blockDescriptor = block;
        // Set to usable in pertinent block categories.
        for (BlockCategory category : categories) {
            this.blockDescriptor.setUsable(category, true);
        }
        // Register instability values.
        InstabilityBlockManager.setInstabilityFactors(this.blockDescriptor.blockstate, instabilityExposed,
                instabilityBase);
    }

    @Override
    public void registerLogic(AgeDirector controller, long seed) {
        ModifierUtils.pushBlock(controller, this.blockDescriptor);
    }

    /**
     * Gets a block property from a string.
     * 
     * @param state The IBlockState that should have the property.
     * @param propertyName The name of the property.
     * @return An IProperty for the input IBlockState and name.
     */
    protected static IProperty parseProperty(IBlockState state, String propertyName) {
        Collection<IProperty<?>> propertyCollection = state.getPropertyKeys();
        for (IProperty<?> property : propertyCollection) {
            if (property.getName().equals(propertyName)) {
                return property;
            }
        }
        return null;
    }

}
