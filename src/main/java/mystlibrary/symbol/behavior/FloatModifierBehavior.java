package mystlibrary.symbol.behavior;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;
import com.xcompwiz.mystcraft.api.symbol.ModifierUtils;
import com.xcompwiz.mystcraft.api.world.AgeDirector;

import mystlibrary.MystLibrary;
import mystlibrary.exception.SymbolBuildException;
import mystlibrary.symbol.SymbolMetadata;
import mystlibrary.symbol.util.SymbolJsonParser;
import net.minecraft.util.ResourceLocation;

/**
 * Pushes a phase to the modifier stack when used. Averages with any existing
 * phases already on the stack.
 */
public final class FloatModifierBehavior implements IAgeSymbolBehavior {

    public static final ResourceLocation NAME = MystLibrary.resource("float_modifier");
    public static final Map<String, String> MODIFIER_TYPES = buildModifierMap();
    private final static String FIELD_NAME_VALUE = "value";
    private final float value;
    private final static String FIELD_NAME_TYPE = "type";
    private final String type;

    public static IAgeSymbolBehavior fromJson(JsonObject json, SymbolMetadata metadata) throws SymbolBuildException {

        FloatModifierBehavior behavior = null;

        float value = SymbolJsonParser.parseFloat(json, FIELD_NAME_VALUE, metadata);
        String type = SymbolJsonParser.parseString(json, FIELD_NAME_TYPE, metadata);

        behavior = new FloatModifierBehavior(value, type);
        return behavior;
    }

    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.addProperty(IAgeSymbolBehavior.BEHAVIOR_ID, NAME.toString());
        json.addProperty(FIELD_NAME_VALUE, value);
        json.addProperty(FIELD_NAME_TYPE, type);
        return json;
    }

    public FloatModifierBehavior(float value, String type) {
        this.value = value;
        this.type = type;
    }

    @Override
    public void registerLogic(AgeDirector controller, long seed) {
        Number previousValue = controller.popModifier(MODIFIER_TYPES.get(this.type)).asNumber();

        if (previousValue == null) {
            previousValue = this.value;
        }

        float averageValue = 0.0F;

        switch (this.type) {
            case "direction":
            case "phase":
                averageValue = ModifierUtils.averageAngles(previousValue.floatValue(), this.value);
                break;
            case "length":
                averageValue = ModifierUtils.averageLengths(previousValue.floatValue(), this.value);
                break;
            default:
                averageValue = ModifierUtils.averageLengths(previousValue.floatValue(), this.value);
                break;
        }

        controller.setModifier(MODIFIER_TYPES.get(this.type), averageValue);
    }

    public static Map<String, String> buildModifierMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("direction", ModifierUtils.ANGLE);
        map.put("length", ModifierUtils.FACTOR);
        map.put("phase", ModifierUtils.PHASE);
        return map;
    }

}
