package mystlibrary.symbol.behavior;

import com.google.gson.JsonObject;
import com.xcompwiz.mystcraft.api.symbol.ModifierUtils;
import com.xcompwiz.mystcraft.api.util.Color;
import com.xcompwiz.mystcraft.api.world.AgeDirector;

import mystlibrary.MystLibrary;
import mystlibrary.exception.SymbolBuildException;
import mystlibrary.symbol.SymbolMetadata;
import mystlibrary.symbol.util.SymbolJsonParser;
import net.minecraft.util.ResourceLocation;

/**
 * Pushes a color to the modifier stack when used. Averages with any existing
 * colors already on the stack.
 */
public final class ColorBehavior implements IAgeSymbolBehavior {

    public static final ResourceLocation NAME = MystLibrary.resource("color");
    private final Color color;
    private static final String FIELD_NAME_RED = "red";
    private static final String FIELD_NAME_GREEN = "green";
    private static final String FIELD_NAME_BLUE = "blue";

    public static IAgeSymbolBehavior fromJson(JsonObject json, SymbolMetadata metadata) throws SymbolBuildException {
        ColorBehavior behavior = null;
        float red = SymbolJsonParser.parseFloat(json, FIELD_NAME_RED, metadata);
        float green = SymbolJsonParser.parseFloat(json, FIELD_NAME_GREEN, metadata);
        float blue = SymbolJsonParser.parseFloat(json, FIELD_NAME_BLUE, metadata);

        behavior = new ColorBehavior(new Color(red, green, blue));

        return behavior;
    }

    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.addProperty(IAgeSymbolBehavior.BEHAVIOR_ID, NAME.toString());
        json.addProperty(FIELD_NAME_RED, color.r);
        json.addProperty(FIELD_NAME_GREEN, color.g);
        json.addProperty(FIELD_NAME_BLUE, color.b);
        return json;
    }

    public ColorBehavior(Color color) {
        this.color = color;
    }

    @Override
    public void registerLogic(AgeDirector controller, long seed) {
        Color previousColor = controller.popModifier(ModifierUtils.COLOR).asColor();

        if (previousColor == null) {
            previousColor = color;
        }

        controller.setModifier(ModifierUtils.COLOR, ModifierUtils.averageColors(color, previousColor));
    }

}
