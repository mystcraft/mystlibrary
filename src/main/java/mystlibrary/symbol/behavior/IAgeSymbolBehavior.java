package mystlibrary.symbol.behavior;

import com.google.gson.JsonObject;
import com.xcompwiz.mystcraft.api.world.AgeDirector;

import mystlibrary.exception.ModMissingException;
import mystlibrary.exception.SymbolBuildException;
import mystlibrary.symbol.SymbolMetadata;

/**
 * Registers symbol logic to the AgeDirector. Used in tandem with AgeSymbol
 * which handles symbol metadata.
 *
 * Instances of this interface are typically deserialized from a JSON object, so
 * be sure to provide a static `fromJSON` method.
 * 
 * Register your behavior by name to `AgeSymbol` in order to create symbols with
 * this behavior from JSON files.
 * 
 * For example:
 * 
 * AgeSymbol.registerBehavior(new ResourceLocation(MystLibrary.MOD_ID, "color"),
 * ColorBehavior::fromJson);
 *
 * @see mystlibrary.symbol.AgeSymbol
 * @see com.xcompwiz.mystcraft.api.symbol.IAgeSymbol#registerLogic
 * @see com.xcompwiz.mystcraft.api.world.AgeDirector
 */
public interface IAgeSymbolBehavior {

    public static final String BEHAVIOR_ID = "behavior_id";

    /**
     * Registers logic to the AgeDirector. See the documentation for
     * IAgeSymbol.registerLogic in the Mystcraft API for more.
     *
     * @see com.xcompwiz.mystcraft.api.symbol.IAgeSymbol#registerLogic
     */
    void registerLogic(AgeDirector controller, long seed);

    /**
     * @return a JSON object representing this behavior.
     */
    JsonObject toJson();

    /**
     * This interface is not meant to be explicitly implemented, but is instead used
     * to allow registration of new behaviors.
     *
     * In order to register a behavior, implement a `fromJson` static method on the
     * behavior, and call it from a lambda provided at registration time. For
     * example:
     *
     * AgeSymbol.registerBehavior( new ResourceLocation(MystLibrary.MOD_ID,
     * "color"), ColorBehavior::fromJson);
     *
     */
    public interface BehaviorFactory {

        /**
         * Builds an instance of a behavior from a JSON Object.
         * 
         * @param json A JSON behavior object
         * @param metadata All the metadata related to this symbol. The behavior may
         *            alter this metadata and those changes will be reflected in the
         *            loaded symbol
         * @return A symbol behavior for use inside an instance of AgeSymbol
         */
        IAgeSymbolBehavior fromJson(JsonObject json, SymbolMetadata metadata)
                throws SymbolBuildException, ModMissingException.BlockSymbol;
    }
}
