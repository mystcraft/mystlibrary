package mystlibrary.symbol;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.JarURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;

import mystlibrary.MystLibrary;
import mystlibrary.exception.ModMissingException;
import mystlibrary.exception.SymbolBuildException;
import mystlibrary.proxy.CommonProxy;
import mystlibrary.symbol.behavior.IAgeSymbolBehavior.BehaviorFactory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistry;

public class SymbolLoader {
    private static final Pattern FILENAME_PATTERN = Pattern.compile(".*?/{0,1}assets/.*?/symbols/[^/]*?.json");
    private static final Pattern CONFIG_PATTERN = Pattern
            .compile(".*?/{0,1}config/mystcraft/mystlibrary/symbols/[^/]*?.json");
    private static final Logger LOGGER = MystLibrary.logger;
    public static final HashMap<ResourceLocation, BehaviorFactory> BEHAVIORS = new HashMap<>();

    private final ClassLoader loader = this.getClass().getClassLoader();

    /**
     * Load AgeSymbols from JSON files matching the pattern `/symbols/[name].json`
     * in either mod asset folders or `config/mystcraft/mystlibrary`.
     */
    public void registerSymbols(IForgeRegistry<IAgeSymbol> registry) {
        File symbolConfigDir = new File(CommonProxy.configDirectory,
                "mystcraft" + File.separator + "mystlibrary" + File.separator + "symbols");
        if (!symbolConfigDir.exists()) {
            symbolConfigDir.mkdirs();
        }

        LOGGER.info("Loading Age Symbols from JSON....");
        List<String> paths = getSymbolPathsFromConfigDir(symbolConfigDir);
        List<AnyFile> files = getModFiles();

        for (AnyFile file : files) {
            if (file.jarFile != null) {
                paths.addAll(getSymbolPathsFromJarFile(file.jarFile));
            } else {
                paths.addAll(getSymbolPathsFromFile(file.file));
            }
        }

        // Used to store missing parent mods for found block symbol JSONs. Gets posted
        // after the next for loop.
        Multimap<String, String> missingMods = HashMultimap.create();

        for (String path : paths) {
            LOGGER.info("Loading symbols from " + path);
            String stringContent = getFileContents(path);
            JsonArray json;

            try {
                json = new JsonParser().parse(stringContent).getAsJsonArray();
            } catch (IllegalStateException e) {
                LOGGER.error("Expected JsonArray but found another type: " + stringContent);
                e.printStackTrace();
                continue;
            } catch (JsonParseException e) {
                LOGGER.error("Parsed text is not valid JSON: " + stringContent);
                e.printStackTrace();
                continue;
            }

            for (JsonElement symbolJson : json) {
                AgeSymbol symbol = null;
                try {
                    symbol = AgeSymbol.fromJson(symbolJson.getAsJsonObject());
                } catch (SymbolBuildException ex) {
                    LOGGER.info("Failed to build symbol \"" + ex.symbolId + "\" from file \""
                            + URI.create(path).getPath().substring(1) + "\":  " + ex.message);
                } catch (ModMissingException.BlockSymbol ex) {
                    missingMods.put(new ResourceLocation(ex.blockId).getResourceDomain(), path);
                }
                if (symbol != null) {
                    if (registry.containsKey(symbol.getRegistryName())) {
                        LOGGER.error("Refusing to register duplicate symbol " + symbol.getRegistryName().toString());
                        continue;
                    }
                    registry.register(symbol);
                    symbol.registerGrammarRules();
                    symbol.registerSymbolRank();
                }
            }
        }

        for (Entry<String, String> entry : missingMods.entries()) {
            LOGGER.info("One or more block symbols from mod \"" + entry.getKey() + "\" added from " + entry.getValue()
                    + " failed to load because the block's mod is not present. Add or enable this mod if you would like to use these symbols.");
        }
    }

    /**
     * Get the string contents of a symbol file.
     *
     * @param path A path to a file either from a loaded mod or from the config
     *            directory
     * @return The contents of the file at the given path
     */
    private String getFileContents(String path) {
        String contents = "";

        try {
            InputStream stream = this.loader.getResourceAsStream(path);
            if (stream == null) {
                path = path.replaceAll("%20", " ");
                stream = this.loader.getResourceAsStream(path);
            }
            StringWriter writer = new StringWriter();
            IOUtils.copy(stream, writer, "UTF-8");
            contents = writer.toString();
        } catch (IOException ex) {
            LOGGER.error("IOException -> Failed to load file contents: " + path);
            LOGGER.error(ex);
        } catch (Exception ex2) {
            LOGGER.error("Exception -> Failed to load file contents: " + path);
            LOGGER.error(ex2);
        }
        return contents;
    }

    /**
     * Get all the symbol file paths contained in a given jar.
     *
     * @return A list of symbol file paths
     */
    private List<String> getSymbolPathsFromJarFile(JarFile jarFile) {
        List<String> paths = new ArrayList<>();
        Enumeration<JarEntry> files = jarFile.entries();

        while (files.hasMoreElements()) {
            JarEntry file = files.nextElement();
            String name = file.getName();
            if (FILENAME_PATTERN.matcher(name).matches()) {
                paths.add(name);
            }
        }

        return paths;
    }

    private List<String> getSymbolPathsFromFile(File file) {
        List<String> paths = new ArrayList<>();

        try {
            Files.walk(file.toPath()).filter(Files::isRegularFile)
                    .filter(path -> FILENAME_PATTERN.matcher(path.toUri().toString()).matches())
                    .forEach(path -> paths.add(path.toUri().toString()));
        } catch (IOException ex) {
            LOGGER.error("Failed to get symbol paths under " + file.toString());
        }

        return paths;
    }

    private List<String> getSymbolPathsFromConfigDir(File file) {
        List<String> paths = new ArrayList<>();
        if (!file.exists()) {
            try {
                file.mkdir();
            } catch (SecurityException ex) {
                LOGGER.error(
                        "Cannot create symbols directory in config/mystcraft/symbols because of security exception.");
                ex.printStackTrace();
            }
        }

        try {
            Files.walk(file.toPath()).filter(Files::isRegularFile)
                    .filter(path -> CONFIG_PATTERN.matcher(path.toUri().toString()).matches())
                    .forEach(path -> paths.add(path.toUri().toString()));
        } catch (IOException ex) {
            LOGGER.error("Failed to get symbol paths under " + file.toString());
        }

        return paths;
    }

    /**
     * @return A list of files that contain an assets folder
     */
    private List<AnyFile> getModFiles() {
        List<URL> urls = getModAssetUrls();
        List<AnyFile> files = new ArrayList<>();

        for (URL url : urls) {
            try {
                URLConnection connection = url.openConnection();
                if (connection instanceof JarURLConnection) {
                    JarURLConnection jarConnection = (JarURLConnection) connection;
                    files.add(new AnyFile(jarConnection.getJarFile()));
                } else {
                    files.add(new AnyFile(Paths.get(url.toURI()).toFile()));
                }
            } catch (IOException ex) {
                LOGGER.error("Failed to load jar file: " + url.toString());
                LOGGER.error(ex);
            } catch (URISyntaxException ex2) {
                LOGGER.error("Failed to load file: " + url.toString());
                LOGGER.error(ex2);
            }
        }

        return files;
    }

    /**
     * @return A list of URLs that point to an assets folder in a mod jar
     */
    private List<URL> getModAssetUrls() {
        List<URL> urls = new ArrayList<>();

        try {
            Enumeration<URL> enumeration = this.loader.getResources("assets/");
            while (enumeration.hasMoreElements()) {
                urls.add(enumeration.nextElement());
            }
        } catch (IOException ex) {
            LOGGER.error("Failed to load mod asset directories");
            LOGGER.error(ex);
        }

        return urls;
    }

    private static class AnyFile {
        private final JarFile jarFile;
        private final File file;

        private AnyFile(JarFile jarFile) {
            this.jarFile = jarFile;
            this.file = null;
        }

        private AnyFile(File file) {
            this.jarFile = null;
            this.file = file;
        }
    }

    /**
     * Registers an IAgeSymbolBehavior to make it available in JSON symbols.
     *
     * <b>Do not use this function.</b> Instead, subscribe to
     * {@code mystlibrary.event.BehaviorRegistrationEvent} and use
     * {@code BehaviorRegistrationEvent::registerBehavior}.
     *
     * For example:
     *
     * event.registerBehavior( new ResourceLocation(MystLibrary.MOD_ID, "color"),
     * (json, metadata) -> ColorBehavior.fromJson(json, metadata) );
     *
     * @param behaviorName - The lookup name for the symbol when deserializing
     * @param factory - A function that builds a behavior from a JSON object and an
     *            instance of SymbolMetadata.
     */
    public void registerBehavior(ResourceLocation behaviorName, BehaviorFactory factory) {
        BEHAVIORS.put(behaviorName, factory);
    }
}
