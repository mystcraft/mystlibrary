package mystlibrary.symbol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.word.WordData;

import mystlibrary.MystLibrary;
import mystlibrary.localization.Localization;
import mystlibrary.symbol.util.SymbolConverter;
import net.minecraft.block.state.IBlockState;
import net.minecraft.world.biome.Biome;

public class PoemWord {
	private static List<String> knownWords = Arrays.asList(WordData.Balance, WordData.Believe, WordData.Celestial,
			WordData.Chain, WordData.Change, WordData.Chaos, WordData.Civilization, WordData.Constraint,
			WordData.Contradict, WordData.Control, WordData.Convey, WordData.Creativity, WordData.Cycle,
			WordData.Dependence, WordData.Discover, WordData.Dynamic, WordData.Elevate, WordData.Encourage,
			WordData.Energy, WordData.Entropy, WordData.Ethereal, WordData.Exist, WordData.Explore, WordData.Flow,
			WordData.Force, WordData.Form, WordData.Future, WordData.Growth, WordData.Harmony, WordData.Honor,
			WordData.Image, WordData.Infinite, WordData.Inhibit, WordData.Intelligence, WordData.Love, WordData.Machine,
			WordData.Merge, WordData.Momentum, WordData.Motion, WordData.Mutual, WordData.Nature, WordData.Nurture,
			WordData.Order, WordData.Possibility, WordData.Power, WordData.Question, WordData.Rebirth,
			WordData.Remember, WordData.Resilience, WordData.Resurrect, WordData.Sacrifice, WordData.Society,
			WordData.Spur, WordData.Static, WordData.Stimulate, WordData.Survival, WordData.Sustain, WordData.System,
			WordData.Terrain, WordData.Time, WordData.Tradition, WordData.Transform, WordData.Void, WordData.Weave,
			WordData.Wisdom);

	public static List<String> getKnownWords() {
		return new ArrayList<String>(knownWords);
	}

	/**
	 * @param poemWord - The word to be localized
	 * @param symbol   - The symbol associated with the given word. This is used to
	 *                 find associated modifiers and other context
	 * @return The best attempt at localizing the given word, or the original word
	 *         if localization attempts fail.
	 */
	public static String localizePoemWord(String poemWord, IAgeSymbol symbol) {
		String unlocalized = "mystcraft.poemword." + poemWord.trim().toLowerCase().replace(" ", "_") + ".name";
		String localized = MystLibrary.proxy.localize(unlocalized);

		if (!localized.equals(unlocalized)) {
			return localized;
		}

		localized = localizeColorWord(poemWord, symbol);

		if (localized == null) {
			localized = localizeBiomeWord(poemWord, symbol);
		}

		if (localized == null) {
			localized = localizeBlockWord(poemWord, symbol);
		}

		if (localized == null) {
			localized = poemWord;
		}

		return localized;
	}

	private static String localizeColorWord(String poemWord, IAgeSymbol symbol) {
		Pattern pattern = Pattern.compile("^modcolor(.*)$");
		String localized = null;

		Matcher matcher = pattern.matcher(poemWord);
		if (matcher.matches()) {
			localized = MystLibrary.proxy.localize("mystcraft.poemword.color." + matcher.group(1) + ".name");
		}

		return localized;
	}

	private static String localizeBiomeWord(String poemWord, IAgeSymbol symbol) {
		Pattern pattern = Pattern.compile("^.*?(\\d+)$");
		String localized = null;

		Matcher matcher = pattern.matcher(poemWord);
		if (matcher.matches()) {
			String biomeId = matcher.group(1);
			Biome biome = SymbolConverter.toBiome(symbol, false);

			if (biome != null) {
				if (biomeId.endsWith(Integer.toString(Biome.getIdForBiome(biome)))) {
					localized = Localization.getLocalizedBiomeName(biome);
				}
			}
		}

		return localized;
	}

	private static String localizeBlockWord(String poemWord, IAgeSymbol symbol) {
		Pattern pattern = Pattern.compile("^modmat_(.*?)_(\\d+)$");
		String localized = null;

		Matcher matcher = pattern.matcher(poemWord);
		if (matcher.matches()) {
			IBlockState blockState = SymbolConverter.toBlockState(symbol, false);

			if (blockState != null) {
				localized = Localization.getLocalizedBlockStateName(blockState);
			}
		}

		return localized;
	}
}
