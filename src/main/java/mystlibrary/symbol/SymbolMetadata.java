package mystlibrary.symbol;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mystlibrary.exception.SymbolBuildException;
import mystlibrary.grammar.GrammarRule;
import mystlibrary.symbol.util.SymbolJsonParser;
import net.minecraft.util.ResourceLocation;

/**
 * Contains information about a symbol beyond its behavior.
 * 
 * While you can create your own instances of this class, it is recommended that
 * you include new symbols as JSON assets rather than code. This mod will
 * automatically discover and register new symbol JSON files from
 * `assets/<modid>/agesymbols/<name>.json`. It will also discover symbols in
 * `config/mystcraft/agesymbols/<name>.json` to allow server customization.
 * 
 * This is mutable to allow the IAgeSymbolBehavior to change things. AgeSymbol
 * holds a private instance of this, at which point it is effectively immutable.
 */
public final class SymbolMetadata {

    public static final String SYMBOL_ID = "symbol_id";
    public static final String POEM = "poem";
    public static final String GRAMMAR_RULES = "grammar_rules";
    public static final String CARD_RANK = "card_rank";
    public static final String INSTABILITY = "instability";
    public static final String MAX_COUNT = "max_count";
    public static final String PER_SYMBOL = "per_symbol";
    public static final String LOCALIZATION = "localization";
    public static final String UNLOCAL_NAME = "unlocal_name";
    public static final String PARAMETERS = "parameters";

    /** The identifier for this symbol */
    public ResourceLocation name;

    /** A list of four poem words to be displayed on the symbol. */
    public List<String> poem = new ArrayList<>();

    /** A list of grammar rules that will apply to this symbol */
    public List<GrammarRule> grammarRules = new ArrayList<>();

    /**
     * A rarity value for the symbol. 0 is extremely common, 3+ is rare.
     * {@code null} means it doesn't show up as loot.
     */
    public Integer cardRank = null;

    /**
     * Optional. The maximum number of symbols that can be used before they add
     * instability. Defaults to null, meaning no limit.
     */
    public Integer maxCount = null;

    /**
     * Optional. The amount of instability per instance after passing the maxCount.
     * Defaults to null meaning no instability.
     */
    public Integer instabilityPerInstance = null;

    /**
     * An unlocalized string template for this symbol. There should be a translation
     * included in the lang file for each value used here.
     */
    public String unlocalName = "";

    /**
     * Optional. A list of parameters to pass into the localization template. These
     * should also be unlocalized strings with translations in the lang file.
     */
    public List<String> localizationParameters = new ArrayList<>();

    public SymbolMetadata() {
    }

    /** Copy constructor, build a new SymbolMetadata from another instance */
    public SymbolMetadata(SymbolMetadata metadata) {
        this.name = new ResourceLocation(metadata.name.toString());
        this.poem = new ArrayList<>(metadata.poem);
        this.grammarRules = new ArrayList<>(metadata.grammarRules);
        this.cardRank = metadata.cardRank;
        this.unlocalName = metadata.unlocalName;
        this.localizationParameters = new ArrayList<>(metadata.localizationParameters);

        if (metadata.maxCount != null) {
            this.maxCount = new Integer(metadata.maxCount.intValue());
        }

        if (metadata.instabilityPerInstance != null) {
            this.instabilityPerInstance = new Integer(metadata.instabilityPerInstance.intValue());
        }
    }

    /**
     * Build an instance of SymbolMetadata from a json object
     * 
     * @throws SymbolBuildException if there is an issue parsing the metadata from a
     *             JSON source.
     */
    public static SymbolMetadata fromJson(JsonObject json) throws SymbolBuildException {
        SymbolMetadata data = new SymbolMetadata();

        // Parse out the symbol name
        String resource = SymbolJsonParser.parseString(json, SYMBOL_ID, data);
        data.name = new ResourceLocation(resource);

        // Parse the symbol poem words
        JsonArray poemWords = SymbolJsonParser.parseJsonArray(json, POEM, data);
        List<String> wordList = new ArrayList<String>();
        for (JsonElement word : poemWords) {
            wordList.add(word.getAsString());
        }
        data.poem = new ArrayList<String>(wordList);

        // Parse out the grammar rules
        JsonArray rules = SymbolJsonParser.parseJsonArray(json, GRAMMAR_RULES, data);
        for (JsonElement rule : rules) {
            data.grammarRules.add(GrammarRule.fromJson(rule.getAsJsonObject(), data, data.name));
        }

        // Parse the card rank
        if (json.has(CARD_RANK)) {
            data.cardRank = Integer.valueOf(SymbolJsonParser.parseInt(json, CARD_RANK, data));
        }

        // Parse instability data
        if (json.has(INSTABILITY)) {
            JsonObject instability = SymbolJsonParser.parseJsonObject(json, INSTABILITY, data);
            if (instability.has(MAX_COUNT)) {
                data.maxCount = Integer.valueOf(SymbolJsonParser.parseInt(instability, MAX_COUNT, data));
            }
            if (instability.has(PER_SYMBOL)) {
                data.instabilityPerInstance = Integer.valueOf(SymbolJsonParser.parseInt(instability, PER_SYMBOL, data));
            }
        }

        // Parse localization data
        if (json.has(LOCALIZATION)) {
            JsonObject localizationJson = SymbolJsonParser.parseJsonObject(json, LOCALIZATION, data);
            if (localizationJson.has(UNLOCAL_NAME)) {
                data.unlocalName = SymbolJsonParser.parseString(localizationJson, UNLOCAL_NAME, data);
                if (localizationJson.has(PARAMETERS)) {
                    JsonArray parameters = SymbolJsonParser.parseJsonArray(localizationJson, PARAMETERS, data);
                    List<String> parameterList = new ArrayList<String>();
                    for (JsonElement parameter : parameters) {
                        parameterList.add(parameter.getAsString());
                    }
                    data.localizationParameters = new ArrayList<String>(parameterList);
                }
            }
        }

        return data;
    }

    /**
     * @return A JsonObject representing this SymbolMetadata
     */
    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        String symbolId = this.name.toString();
        json.addProperty(SYMBOL_ID, symbolId);

        JsonArray poemArray = new JsonArray();
        json.add(POEM, poemArray);
        for (String word : this.poem) {
            poemArray.add(word);
        }

        json.addProperty(CARD_RANK, this.cardRank);

        JsonObject instability = new JsonObject();
        instability.addProperty(MAX_COUNT, this.maxCount);
        instability.addProperty(PER_SYMBOL, this.instabilityPerInstance);
        json.add(INSTABILITY, instability);

        JsonObject localization = new JsonObject();
        localization.addProperty(UNLOCAL_NAME, this.unlocalName);
        JsonArray params = new JsonArray();
        for (String parameter : this.localizationParameters) {
            params.add(parameter);
        }
        localization.add(PARAMETERS, params);
        json.add(LOCALIZATION, localization);

        JsonArray grammarRules = new JsonArray();
        for (GrammarRule rule : this.grammarRules) {
            JsonObject object = new JsonObject();
            object.addProperty("parent", rule.parent.toString());
            object.addProperty("rank", rule.rank);
            grammarRules.add(object);
        }
        json.add(GRAMMAR_RULES, grammarRules);

        return json;
    }
}
