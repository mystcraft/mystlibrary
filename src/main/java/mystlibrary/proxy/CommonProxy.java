package mystlibrary.proxy;

import java.io.File;

import mystlibrary.MystLibrary;
import mystlibrary.event.EventHandler;
import mystlibrary.mystcraftapi.MystcraftApiWrapper;
import mystlibrary.word.WordLoader;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy {
    public static File configDirectory;

    public void preInit(FMLPreInitializationEvent event) {
        configDirectory = event.getModConfigurationDirectory();

        MystLibrary.mystcraft = new MystcraftApiWrapper();

        MinecraftForge.EVENT_BUS.register(new EventHandler());

        new WordLoader().registerWords();
    }

    public void init(FMLInitializationEvent event) {
    }

    public void postInit(FMLPostInitializationEvent event) {
    }

    public String localize(String unlocalizedString, Object... parameters) {
        return I18n.format(unlocalizedString, parameters);
    }

}
