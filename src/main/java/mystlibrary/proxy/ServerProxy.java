package mystlibrary.proxy;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.SERVER)
public class ServerProxy extends CommonProxy {
    @Override
    public String localize(String unlocalizedString, Object... parameters) {
        return unlocalizedString;
    }
}