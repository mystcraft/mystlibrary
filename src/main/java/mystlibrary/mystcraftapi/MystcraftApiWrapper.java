package mystlibrary.mystcraftapi;

import org.apache.logging.log4j.Logger;

import com.xcompwiz.mystcraft.api.APIInstanceProvider;
import com.xcompwiz.mystcraft.api.MystObjects;
import com.xcompwiz.mystcraft.api.exception.APIUndefined;
import com.xcompwiz.mystcraft.api.exception.APIVersionRemoved;
import com.xcompwiz.mystcraft.api.exception.APIVersionUndefined;
import com.xcompwiz.mystcraft.api.hook.DimensionAPI;
import com.xcompwiz.mystcraft.api.hook.GrammarAPI;
import com.xcompwiz.mystcraft.api.hook.InstabilityAPI;
import com.xcompwiz.mystcraft.api.hook.InstabilityFactory;
import com.xcompwiz.mystcraft.api.hook.ItemFactory;
import com.xcompwiz.mystcraft.api.hook.LinkPropertyAPI;
import com.xcompwiz.mystcraft.api.hook.LinkingAPI;
import com.xcompwiz.mystcraft.api.hook.PageAPI;
import com.xcompwiz.mystcraft.api.hook.RenderAPI;
import com.xcompwiz.mystcraft.api.hook.SymbolAPI;
import com.xcompwiz.mystcraft.api.hook.SymbolFactory;
import com.xcompwiz.mystcraft.api.hook.SymbolValuesAPI;
import com.xcompwiz.mystcraft.api.hook.WordAPI;

import mystlibrary.MystLibrary;

/**
 * Provides direct access to Mystcraft APIs, or null if they are not available.
 *
 * This class exports the latest version of each API. You can get your own
 * instances via MystObjects.entryPoint.getProviderInstance()
 */
public final class MystcraftApiWrapper {
    private final Logger log;

    public final DimensionAPI dimension;
    public final GrammarAPI grammar;
    public final InstabilityAPI instability;
    public final InstabilityFactory instabilityFactory;
    public final ItemFactory itemFactory;
    public final LinkingAPI linking;
    public final LinkPropertyAPI linkProperty;
    public final PageAPI page;
    public final RenderAPI render;
    public final SymbolAPI symbol;
    public final SymbolFactory symbolFactory;
    public final SymbolValuesAPI symbolValues;
    public final WordAPI word;

    public MystcraftApiWrapper() {
        log = MystLibrary.logger;
        log.info("Initializing Mystcraft APIs");

        APIInstanceProvider provider;
        try {
            provider = MystObjects.entryPoint.getProviderInstance();
        } catch (Exception ex) {
            log.error("Failed to get Mystcraft API Provider!");
            log.error(ex.getMessage());

            provider = new FakeInstanceProvider();
        }

        dimension = (DimensionAPI) getInstanceFromProvider(provider, "dimension-1");
        grammar = (GrammarAPI) getInstanceFromProvider(provider, "grammar-1");
        instability = (InstabilityAPI) getInstanceFromProvider(provider, "instability-1");
        instabilityFactory = (InstabilityFactory) getInstanceFromProvider(provider, "instabilityfact-1");
        itemFactory = (ItemFactory) getInstanceFromProvider(provider, "itemfact-1");
        linking = (LinkingAPI) getInstanceFromProvider(provider, "linking-1");
        linkProperty = (LinkPropertyAPI) getInstanceFromProvider(provider, "linkingprop-1");
        page = (PageAPI) getInstanceFromProvider(provider, "page-1");
        render = (RenderAPI) getInstanceFromProvider(provider, "render-1");
        symbol = (SymbolAPI) getInstanceFromProvider(provider, "symbol-1");
        symbolFactory = (SymbolFactory) getInstanceFromProvider(provider, "symbolfact-1");
        symbolValues = (SymbolValuesAPI) getInstanceFromProvider(provider, "symbolvals-1");
        word = (WordAPI) getInstanceFromProvider(provider, "word-1");
    }

    private Object getInstanceFromProvider(APIInstanceProvider provider, String instanceName) {
        try {
            return provider.getAPIInstance(instanceName);
        } catch (APIVersionRemoved e1) {
            log.error("API version removed!");
            log.error(e1.getMessage());
        } catch (APIVersionUndefined e2) {
            log.error("API version undefined!");
            log.error(e2.getMessage());
        } catch (APIUndefined e3) {
            MystLibrary.logger.error("API undefined!");
            MystLibrary.logger.error(e3.getMessage());
        }

        return null;
    }

    /**
     * Return nulls when Mystcraft is not installed
     */
    private final class FakeInstanceProvider implements APIInstanceProvider {
        @Override
        public Object getAPIInstance(String api) throws APIUndefined, APIVersionUndefined, APIVersionRemoved {
            return null;
        }
    }
}
