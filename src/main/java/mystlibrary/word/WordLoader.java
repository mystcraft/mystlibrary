package mystlibrary.word;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.xcompwiz.mystcraft.api.word.DrawableWord;

import mystlibrary.MystLibrary;
import mystlibrary.proxy.CommonProxy;
import net.minecraft.util.ResourceLocation;

public class WordLoader {

    private static final Pattern FILENAME_PATTERN = Pattern.compile(".*?/{0,1}assets/.*?/words/[^/]*?.json");
    private static final Pattern CONFIG_PATTERN = Pattern
            .compile(".*?/{0,1}config/mystcraft/mystlibrary/words/[^/]*?.json");
    private static final Logger LOGGER = MystLibrary.logger;
    private final ClassLoader loader = this.getClass().getClassLoader();

    /**
     * Load custom poem words from JSON files matching the pattern
     * `/words/[name].json` in either mod asset folders or
     * `config/mystcraft/mystlibrary`. You should not need to call this method. If
     * you want to write code to register words yourself, you should do so during
     * preInit.
     */
    public void registerWords() {

        File symbolConfigDir = new File(CommonProxy.configDirectory,
                "mystcraft" + File.separator + "mystlibrary" + File.separator + "words");

        if (!symbolConfigDir.exists()) {
            symbolConfigDir.mkdirs();
        }

        LOGGER.info("Loading words from JSON....");
        List<String> paths = getSymbolPathsFromConfigDir(symbolConfigDir);
        List<AnyFile> files = getModFiles();

        for (AnyFile file : files) {
            if (file.jarFile != null) {
                paths.addAll(getSymbolPathsFromJarFile(file.jarFile));
            } else {
                paths.addAll(getSymbolPathsFromFile(file.file));
            }
        }

        for (String path : paths) {
            LOGGER.info("Loading words from " + path);
            String stringContent = getFileContents(path);
            JsonArray json;

            try {
                json = new JsonParser().parse(stringContent).getAsJsonArray();
            } catch (IllegalStateException e) {
                LOGGER.error("Expected JsonArray but found another type: " + stringContent);
                e.printStackTrace();
                continue;
            } catch (JsonParseException e) {
                LOGGER.error("Parsed text is not valid JSON: " + stringContent);
                e.printStackTrace();
                continue;
            }

            for (JsonElement jsonElement : json) {

                // Parse whole element.
                JsonObject jsonObject = new JsonObject();
                try {
                    jsonObject = jsonElement.getAsJsonObject();
                } catch (IllegalStateException e) {
                    LOGGER.info("Skipping word. Expected JsonObject but element is of another type: "
                            + jsonElement.toString());
                    continue;
                }

                // Parse "word" field.
                JsonElement wordElement = jsonObject.get("word");
                if (wordElement == null) {
                    LOGGER.info("Skipping word. Cannot find field \"word\" in JSON: " + jsonElement.toString());
                    continue;
                }
                String word = "";
                try {
                    word = wordElement.getAsString();
                } catch (ClassCastException e) {
                    LOGGER.info(
                            "Skipping word. Value of field \"word\" is not a JsonPrimitive and is not a valid string value: "
                                    + jsonElement.toString());
                    continue;
                } catch (IllegalStateException e) {
                    LOGGER.info("Skipping word. Value of field \"word\" is an array but contains more than one value: "
                            + jsonElement.toString());
                    continue;
                }

                // Parse "arcs" field.
                JsonElement arcsElement = jsonObject.get("arcs");
                if (arcsElement == null) {
                    LOGGER.info("Skipping word. Cannot find field \"arcs\" in JSON: " + jsonElement.toString());
                    continue;
                }
                JsonArray numbersArray = new JsonArray();
                try {
                    numbersArray = arcsElement.getAsJsonArray();
                } catch (IllegalStateException e) {
                    LOGGER.info("Skipping word. Value of field \"arcs\" is not a JsonArray: " + jsonElement.toString());
                    continue;
                }
                List<Integer> numbers = new ArrayList<Integer>();
                for (JsonElement element : numbersArray) {
                    Integer integer = null;
                    try {
                        integer = Integer.valueOf(element.getAsInt());
                    } catch (ClassCastException e) {
                        LOGGER.info(
                                "Skipping arc. Value of array element is not a JsonPrimitive and is not a valid integer value: "
                                        + jsonElement.toString());
                        continue;
                    } catch (IllegalStateException e) {
                        LOGGER.info(
                                "Skipping arc. Value of array element is an array but contains more than one value: "
                                        + jsonElement.toString());
                        continue;
                    }
                    numbers.add(integer);
                }

                // Parse "colors" field.
                List<Integer> colors = new ArrayList<Integer>();
                JsonElement colorsElement = jsonObject.get("colors");
                if (colorsElement != null) {
                    JsonArray colorsArray = new JsonArray();
                    try {
                        colorsArray = colorsElement.getAsJsonArray();
                    } catch (IllegalStateException e) {
                        LOGGER.info("Coloring all arcs black. Value of field \"colors\" is not a JsonArray: "
                                + jsonElement.toString());
                    }
                    for (JsonElement element : colorsArray) {
                        Integer integer = null;
                        try {
                            integer = Integer.valueOf(element.getAsInt());
                        } catch (ClassCastException e) {
                            LOGGER.info(
                                    "Coloring arc black. Value of array element is not a JsonPrimitive and is not a valid integer value: "
                                            + jsonElement.toString());
                            integer = 0;
                        } catch (IllegalStateException e) {
                            LOGGER.info(
                                    "Coloring arc black. Value of array element is an array but contains more than one value: "
                                            + jsonElement.toString());
                            integer = 0;
                        }
                        colors.add(integer);
                    }
                }
                if (colors.size() == 0) {
                    for (int i = 0; i < numbers.size(); i++) {
                        colors.add(0);
                    }
                }

                // Parse "texture" field;
                String texture = "";
                JsonElement textureElement = jsonObject.get("texture");
                if (textureElement != null) {
                    try {
                        texture = textureElement.getAsString();
                    } catch (ClassCastException e) {
                        LOGGER.info(
                                "Using default texture. Value of field \"texture\" is not a JsonPrimitive and is not a valid string value: "
                                        + jsonElement.toString());
                    } catch (IllegalStateException e) {
                        LOGGER.info(
                                "Using default texture. Value of field \"texture\" is an array but contains more than one value: "
                                        + jsonElement.toString());
                    }
                }

                DrawableWord drawableWord = new DrawableWord();
                for (int i = 0; i < numbers.size(); i++) {
                    drawableWord.addDrawComponent(numbers.get(i), colors.get(i));
                }
                if (!texture.trim().isEmpty()) {
                    drawableWord.setImageSource(new ResourceLocation(texture));
                }
                MystLibrary.mystcraft.word.registerWord(word, drawableWord);

            }
        }

    }

    private String getFileContents(String path) {
        String contents = "";

        try {
            InputStream stream = this.loader.getResourceAsStream(path);
            StringWriter writer = new StringWriter();
            IOUtils.copy(stream, writer, "UTF-8");
            contents = writer.toString();
        } catch (IOException ex) {
            LOGGER.error("Failed to load file contents: " + path);
            LOGGER.error(ex);
        } catch (Exception ex2) {
            LOGGER.error("Failed to load file contents: " + path);
            LOGGER.error(ex2);
        }

        return contents;
    }

    private List<String> getSymbolPathsFromConfigDir(File file) {
        List<String> paths = new ArrayList<>();
        if (!file.exists()) {
            try {
                file.mkdir();
            } catch (SecurityException ex) {
                LOGGER.error("Cannot create words directory in config/mystcraft/words because of security exception.");
                ex.printStackTrace();
            }
        }

        try {
            Files.walk(file.toPath()).filter(Files::isRegularFile)
                    .filter(path -> CONFIG_PATTERN.matcher(path.toUri().toString()).matches())
                    .forEach(path -> paths.add(path.toUri().toString()));
        } catch (IOException ex) {
            LOGGER.error("Failed to get word paths under " + file.toString());
        }

        return paths;
    }

    private List<AnyFile> getModFiles() {
        List<URL> urls = getModAssetUrls();
        List<AnyFile> files = new ArrayList<>();

        for (URL url : urls) {
            try {
                URLConnection connection = url.openConnection();
                if (connection instanceof JarURLConnection) {
                    JarURLConnection jarConnection = (JarURLConnection) connection;
                    files.add(new AnyFile(jarConnection.getJarFile()));
                } else {
                    files.add(new AnyFile(Paths.get(url.toURI()).toFile()));
                }
            } catch (IOException ex) {
                LOGGER.error("Failed to load jar file: " + url.toString());
                LOGGER.error(ex);
            } catch (URISyntaxException ex2) {
                LOGGER.error("Failed to load file: " + url.toString());
                LOGGER.error(ex2);
            }
        }

        return files;
    }

    private List<URL> getModAssetUrls() {
        List<URL> urls = new ArrayList<>();

        try {
            Enumeration<URL> enumeration = this.loader.getResources("assets/");
            while (enumeration.hasMoreElements()) {
                urls.add(enumeration.nextElement());
            }
        } catch (IOException ex) {
            LOGGER.error("Failed to load mod asset directories");
            LOGGER.error(ex);
        }

        return urls;
    }

    private List<String> getSymbolPathsFromJarFile(JarFile jarFile) {
        List<String> paths = new ArrayList<>();
        Enumeration<JarEntry> files = jarFile.entries();

        while (files.hasMoreElements()) {
            JarEntry file = files.nextElement();
            String name = file.getName();
            if (FILENAME_PATTERN.matcher(name).matches()) {
                paths.add(name);
            }
        }

        return paths;
    }

    private List<String> getSymbolPathsFromFile(File file) {
        List<String> paths = new ArrayList<>();

        try {
            Files.walk(file.toPath()).filter(Files::isRegularFile)
                    .filter(path -> FILENAME_PATTERN.matcher(path.toUri().toString()).matches())
                    .forEach(path -> paths.add(path.toUri().toString()));
        } catch (IOException ex) {
            LOGGER.error("Failed to get word paths under " + file.toString());
        }

        return paths;
    }

    private static class AnyFile {
        private final JarFile jarFile;
        private final File file;

        private AnyFile(JarFile jarFile) {
            this.jarFile = jarFile;
            this.file = null;
        }

        private AnyFile(File file) {
            this.jarFile = null;
            this.file = file;
        }
    }
}
