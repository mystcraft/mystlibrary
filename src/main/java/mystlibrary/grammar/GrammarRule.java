package mystlibrary.grammar;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mystlibrary.exception.SymbolBuildException;
import mystlibrary.symbol.SymbolMetadata;
import mystlibrary.symbol.util.SymbolJsonParser;
import net.minecraft.util.ResourceLocation;

/**
 * Represents a rule for use in the Mystcraft Context Free Grammar.
 * 
 * These rules are used during age generation to validate and fill in symbols.
 */
public final class GrammarRule {

    private static final String PARENT = "parent";
    private static final String RANK = "rank";
    private static final String CHILDREN = "children";
    public final ResourceLocation parent;
    public final Integer rank;
    private final ResourceLocation[] children;

    public static GrammarRule fromJson(JsonObject json, SymbolMetadata metadata) throws SymbolBuildException {
        GrammarRule rule = null;
        List<ResourceLocation> children = new ArrayList<>();
        JsonArray childrenArray = SymbolJsonParser.parseJsonArray(json, CHILDREN, metadata);
        for (JsonElement child : childrenArray) {
            children.add(new ResourceLocation(child.getAsString()));
        }

        rule = GrammarRule.fromJson(json, metadata, children.toArray(new ResourceLocation[0]));

        return rule;
    }

    public static GrammarRule fromJson(JsonObject json, SymbolMetadata metadata, ResourceLocation... children)
            throws SymbolBuildException {
        GrammarRule rule = null;
        String parentString = SymbolJsonParser.parseString(json, PARENT, metadata);
        ResourceLocation parent = new ResourceLocation(parentString);
        JsonElement element = json.get(RANK);
        Integer rank = null;
        if (element != null) {
            rank = Integer.valueOf(SymbolJsonParser.parseInt(json, RANK, metadata));
        }

        rule = new GrammarRule(parent, rank, children);

        return rule;
    }

    public GrammarRule(ResourceLocation parent, Integer rank, ResourceLocation... children) {
        this.parent = parent;
        this.rank = rank;
        this.children = children;
    }

    public ResourceLocation[] getChildren() {
        return children.clone();
    }
}