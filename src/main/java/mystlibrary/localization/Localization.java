package mystlibrary.localization;

import org.apache.logging.log4j.Logger;

import mystlibrary.MystLibrary;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.biome.Biome;

public final class Localization {

    private static final Logger LOG = MystLibrary.logger;

    private Localization() {
    }

    public static String getLocalizedBiomeName(Biome biome) {
        String name = null;
        if (biome != null) {
            name = biome.getBiomeName();
        }
        return name;
    }

    public static String getLocalizedBlockStateName(IBlockState state) {
        String localized = null;

        if (state != null) {
            Block block = state.getBlock();
            ItemStack stack = getItemStackFromBlockState(state);

            if (stack != null) {
                localized = stack.getDisplayName();
            }

            if (localized == null) {
                try {
                    Item item = Item.getItemFromBlock(block);
                    String unlocal = item.getUnlocalizedName();

                    localized = MystLibrary.proxy.localize(unlocal);

                    if (localized.equals(unlocal)) {
                        localized = null;
                    }
                } catch (Exception ex) {
                }
            }

            if (localized == null) {
                localized = block.getLocalizedName();
            }
        }

        return localized;
    }

    public static String getUnlocalizedBlockStateName(IBlockState state) {
        String name = null;

        if (state != null) {
            Block block = state.getBlock();
            ItemStack stack = getItemStackFromBlockState(state);

            if (stack != null) {
                name = stack.getUnlocalizedName();
            }

            if (name == null) {
                try {
                    Item item = Item.getItemFromBlock(block);
                    name = item.getUnlocalizedName();
                } catch (Exception ex) {
                }
            }

            if (name == null) {
                name = block.getUnlocalizedName();
            }
        }

        return name;
    }

    public static ItemStack getItemStackFromBlockState(IBlockState state) {
        ItemStack stack = ItemStack.EMPTY;
        Block block = state.getBlock();

        try {
            stack = block.getPickBlock(state, null, null, null, null);
        } catch (Exception ex) {
            LOG.error("Block::getPickBlock failed for IBlockState \"" + state.toString()
                    + "\". Attempting to create new ItemStack instance.");
        }

        if (stack == null || stack.equals(ItemStack.EMPTY)) {
            try {
                int meta = state.getBlock().getMetaFromState(state);
                stack = new ItemStack(state.getBlock(), 1, meta);
            } catch (Exception ex) {
                LOG.error("Failed to create new ItemStack instance from IBlockState \"" + state.toString()
                        + "\". Returning empty ItemStack.");
            }
        }

        if (stack == null) {
            stack = ItemStack.EMPTY;
        }

        return stack;
    }

}
