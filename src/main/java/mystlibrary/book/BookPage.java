package mystlibrary.book;

import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.collect.Lists;
import com.xcompwiz.mystcraft.api.MystObjects;
import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

/**
 * A substitute for Mystcraft's Page that can be used to make books in
 * {@link mystlibrary.book.BookGenerator} Cannot be instantiated; use one of the
 * factories instead.
 */
public final class BookPage {

    private PageType type = null;
    private ResourceLocation symbol = null;
    private List<String> panelEffects = null;

    private BookPage(PageType pageType, ResourceLocation location, List<String> effects) {
        this.setType(pageType);
        this.setSymbol(location);
        this.setPanelEffects(effects);
    }

    /**
     * Creates a blank page.
     * 
     * @return A blank BookPage.
     */
    public static BookPage blankPage() {
        return new BookPage(PageType.BLANK_PAGE, null, null);
    }

    /**
     * Creates a page with a Symbol on it.
     * 
     * @param The ResourceLocation of the Symbol.
     * @return A Symbol BookPage.
     */
    public static BookPage symbolPage(@Nonnull ResourceLocation location) {
        return new BookPage(PageType.SYMBOL, location, null);
    }

    /**
     * Creates a page with a Symbol on it.
     * 
     * @param The IAgeSymbol to put on the page.
     * @return A Symbol BookPage.
     */
    public static BookPage symbolPage(@Nonnull IAgeSymbol symbol) {
        return symbolPage(symbol.getRegistryName());
    }

    /**
     * Creates a page with a Link Panel on it.
     * 
     * @param The effects that should be applied to the panel.
     * @return A Link Panel BookPage.
     */
    public static BookPage linkPanelPage(@Nonnull List<String> effects) {
        return new BookPage(PageType.LINKING_PANEL, null, effects);
    }

    /**
     * Creates a page with a Link Panel on it.
     * 
     * @param The effects that should be applied to the panel.
     * @return A Link Panel BookPage.
     */
    public static BookPage linkPanelPage(@Nonnull String... effects) {
        return linkPanelPage(Lists.newArrayList(effects));
    }

    /**
     * 
     * @return The ResourceLocation of the IAgeSymbol on this page if it is a Symbol
     *         BookPage. Otherwise returns <b>null</b>.
     */
    public ResourceLocation getSymbol() {
        if (this.getType().equals(PageType.SYMBOL)) {
            return this.symbol;
        }

        return null;
    }

    private void setSymbol(ResourceLocation location) {
        this.symbol = location;
    }

    /**
     * 
     * @return The effects applied to the Link Panel on this page if it is a Link
     *         Panel BookPage. Otherwise returns an empty list.
     */
    public List<String> getPanelEffects() {
        if (this.getType().equals(PageType.LINKING_PANEL)) {
            return this.panelEffects;
        }

        return null;
    }

    private void setPanelEffects(List<String> effects) {
        this.panelEffects = effects;
    }

    /**
     * 
     * @return The type of page this is. Will be BLANK, SYMBOL, or LINKING_PANEL.
     */
    public PageType getType() {
        return this.type;
    }

    private void setType(PageType pageType) {
        this.type = pageType;
    }

    /**
     * Used to convert a BookPage into a Page ItemStack.
     * 
     * @return An ItemStack that represents this BookPage. Will return
     *         ItemStack.EMPTY if there is an error.
     */
    public ItemStack asItemStack() {

        ItemStack pageStack = BookGenerator.getMystcraftStack(MystObjects.Items.page);
        if (pageStack.isEmpty()) {
            return pageStack;
        }

        NBTTagCompound nbt = new NBTTagCompound();

        switch (this.getType()) {
            case BLANK_PAGE:
                nbt = BookGenerator.generateBlankPage();
                break;
            case SYMBOL:
                nbt = BookGenerator.generateLinkPanelPage(this.getPanelEffects());
                break;
            case LINKING_PANEL:
                nbt = BookGenerator.generateSymbolPage(this.getSymbol());
                break;
        }

        pageStack.setTagCompound(nbt);
        return pageStack;
    }

    /**
     * The types of pages currently supported.
     */
    public enum PageType {
        BLANK_PAGE,
        SYMBOL,
        LINKING_PANEL;
    }

}
