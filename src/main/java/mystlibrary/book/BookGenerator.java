package mystlibrary.book;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.xcompwiz.mystcraft.api.MystObjects;
import com.xcompwiz.mystcraft.api.linking.ILinkInfo;

import mystlibrary.MystLibrary;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;

public final class BookGenerator {

    private BookGenerator() {
    }

    /**
     * Generates an ItemStack for a Descriptive Book <i>(mystcraft:agebook)</i>.
     * 
     * @param pages A list of pages to include in the book. These can be blank,
     *            symbols, or linking panels.
     * @param bookName The name of the book.
     * @param bookAuthors The authors of the book.
     * @return ItemStack of a Descriptive Book or an empty ItemStack if
     *         <i>mystcraft:agebook</i> is not present.
     */
    public static ItemStack generateDescriptiveBook(List<BookPage> pages, @Nonnull String bookName,
            @Nonnull List<String> bookAuthors) {

        ItemStack bookStack = getMystcraftStack(MystObjects.Items.descriptive_book);
        if (bookStack.isEmpty()) {
            return bookStack;
        }
        bookStack.setItemDamage(0);

        NBTTagCompound compoundDesBook = new NBTTagCompound();
        compoundDesBook.setFloat("MaxHealth", 10);
        compoundDesBook.setFloat("damage", 0);
        compoundDesBook.setString("DisplayName", bookName);

        NBTTagList listAuthors = new NBTTagList();
        for (String author : bookAuthors) {
            NBTTagString stringAuthor = new NBTTagString(author);
            listAuthors.appendTag(stringAuthor);
        }
        compoundDesBook.setTag("Authors", listAuthors);

        NBTTagList listPages = new NBTTagList();

        for (BookPage page : pages) {
            if (page.getSymbol() != null) {
                NBTTagCompound compoundSymbol = generateSymbolPage(page.getSymbol());
                listPages.appendTag(compoundSymbol);
            } else if (page.getPanelEffects() != null) {
                NBTTagCompound compoundLinkPanel = generateLinkPanelPage(page.getPanelEffects());
                listPages.appendTag(compoundLinkPanel);
            } else {
                NBTTagCompound compoundBlankPage = generateBlankPage();
                listPages.appendTag(compoundBlankPage);
            }
        }

        compoundDesBook.setTag("Pages", listPages);
        bookStack.setTagCompound(compoundDesBook);

        return bookStack;
    }

    public static NBTTagCompound generateLinkPanelPage(List<String> effects) {
        NBTTagCompound compoundPage = generateBlankPage();
        NBTTagList properties = new NBTTagList();
        for (String effect : effects) {
            NBTTagString effectName = new NBTTagString(effect);
            properties.appendTag(effectName);
        }
        NBTTagCompound panel = new NBTTagCompound();
        panel.setTag("properties", properties);
        NBTTagCompound tag = new NBTTagCompound();
        tag.setTag("linkpanel", panel);
        compoundPage.setTag("tag", tag);
        return compoundPage;
    }

    @Nullable
    public static NBTTagCompound generateSymbolPage(ResourceLocation location) {
        NBTTagCompound compoundPage = generateBlankPage();
        NBTTagCompound tag = new NBTTagCompound();
        String id = location.toString();
        tag.setString("symbol", id);
        compoundPage.setTag("tag", tag);
        return compoundPage;
    }

    public static NBTTagCompound generateBlankPage() {
        NBTTagCompound compoundPage = new NBTTagCompound();
        compoundPage.setByte("Count", (byte) 1);
        compoundPage.setShort("Damage", (short) 0);
        compoundPage.setString("id", "mystcraft:page");
        NBTTagCompound tag = new NBTTagCompound();
        compoundPage.setTag("tag", tag);
        return compoundPage;
    }

    /**
     * Generates an ItemStack for a Linking Book <i>(mystcraft:linkbook)</i>. The
     * book is generated as a Linking Book to the Overworld spawn before the
     * provided arguments are considered, so if any of them are {@code null}, they
     * will be the Overworld spawn defaults.
     * 
     * @param blockPos The BlockPos where the user will spawn.
     * @param yaw The yaw of the Entity after they link.
     * @param dimension The integer ID of the dimension the book leads to.
     * @param displayName The name that will be displayed on the inside cover of the
     *            book.
     * @param effects The effects that should ({@code true}) or should not
     *            ({@code false}) be applied to the linking panel. It is not
     *            necessary to list {@code false} effects.
     * @return ItemStack of a Linking Book or an empty ItemStack if
     *         <i>mystcraft:linkbook</i> is not present.
     */
    public static ItemStack generateLinkingBook(BlockPos blockPos, float yaw, int dimension, String displayName,
            Map<String, Boolean> effects) {
        ItemStack bookStack = getMystcraftStack(MystObjects.Items.linkbook);
        if (bookStack.isEmpty()) {
            return bookStack;
        }
        ILinkInfo info = MystLibrary.mystcraft.linking.createLinkInfo(null);
        info.setDimensionUID(dimension);
        info.setTargetUUID(new UUID(dimension, 0L));
        info.setSpawnYaw(yaw);
        if (displayName != null) {
            info.setDisplayName(displayName);
        }
        if (blockPos != null) {
            info.setSpawn(blockPos);
        }
        if (effects != null) {
            for (Entry<String, Boolean> flag : effects.entrySet()) {
                info.setFlag(flag.getKey(), flag.getValue());
            }
        }
        bookStack.setTagCompound(info.getTagCompound());
        return bookStack;
    }

    public static ItemStack getMystcraftStack(String name) {
        ItemStack stack = ItemStack.EMPTY;
        String id = MystObjects.MystcraftModId + ":" + name;
        Item item = Item.getByNameOrId(id);
        if (item != null) {
            stack = new ItemStack(item);
        } else {
            MystLibrary.logger.error("Item \"" + id + "\" is not present. Returning empty ItemStack.");
        }

        return stack;
    }

}